const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');

const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Article = require('../models/Article');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPathPhotos);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', [auth, upload.single('image'), permit('admin')], async (req, res) => {
  console.log(req.body);
  const articleData = {
    user_id: req.user._id,
    category_id: req.body.category_id,
    title: req.body.title,
    description: req.body.description,
    date: new Date().toLocaleString('ru-RU')
  };

  if (req.file) {
    articleData.image = req.file.filename;
  }

  const article = await new Article(articleData);

  try {
    await article.save();
    return res.send(article);
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.get('/', async (req, res) => {
  try {
    const articles = await Article.find().populate('category_id').populate({ path: 'user_id', selected: 'username, role', });
    return res.send(articles)
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.get('/:id', async (req, res) => {
  try {
    const article = await Article.findById(req.params.id).populate('category_id').populate({ path: 'user_id', selected: 'username, role', });
    return res.send(article)
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.get('/category/:id', async (req, res) => {
  try {
    const article = await Article.find({category_id: req.params.id}).populate({ path: 'user_id', selected: 'username, role', });
    return res.send(article)
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
  try {
    await Article.deleteOne({_id: req.params.id});

    res.send({message: "OK"})

  } catch (e) {
    return res.status(500).send(e);
  }
});

router.post('/:id', [auth, upload.single('image'), permit('admin')], async (req, res) => {
  try {
    const body = req.body;
    if (req.file) {
      body.image = req.file.filename;
    } else {
      body.image = req.body.image
    }
    await Article.updateOne({_id: req.params.id},
      {$set:
          {
            title: body.title,
            description: body.description,
            image: body.image,
            category_id: body.category_id
          }
      });
    res.send({"message": "ok"})
  } catch (error) {
    console.log(error);
    return res.status(400).send(error)
  }
});


module.exports = router;
