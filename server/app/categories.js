const express = require('express');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Category = require('../models/Category');
const Article = require('../models/Article');

const router = express.Router();

router.post('/', [auth, permit('admin')], async (req, res) => {
  const categoryData = {
    title: req.body.title,
  };

  const category = await new Category(categoryData);

  try {
    await category.save();
    return res.send(category);
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.get('/', async (req, res) => {
  try {
    const category = await Category.find();
    return res.send(category)
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.get('/:id', async (req, res) => {
  try {
    const category = await Category.findOne({_id: req.params.id});
    return res.send(category)
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
  try {
    await Category.find({_id: req.params.id}).then(result => {
      result.forEach((category) => {
        Article.deleteMany({category_id: category._id})
          .catch(error => res.status(400).send(error))
      })
    });
    await Category.deleteOne({_id: req.params.id});

    res.send({message: "OK"})

  } catch (e) {
    return res.status(500).send(e);
  }
});

router.post('/:id', [auth, permit('admin')], async (req, res) => {
  try {
    await req.body;
    await Category.updateOne({_id: req.params.id},
      {$set:
          {
            title: req.body.title,
          }
      });
    res.send({"message": "ok"})
  } catch (error) {
    console.log(error);
    return res.status(400).send(error)
  }
});

module.exports = router;
