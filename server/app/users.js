const express = require('express');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
  const user = new User({
    username: req.body.username,
    password: req.body.password,
    role: req.body.role
  });
  user.generateToken();

  try {
    await user.save();
    return res.send({message: 'User registered', user});
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.post('/sessions', async (req, res) => {
  const user = await User.findOne({username: req.body.username});

  if (!user) {
    return res.status(400).send({error: 'User does not exist'});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(400).send({error: 'Password incorrect'});
  }

  user.generateToken();

  await user.save();

  res.send({message: 'Login successful', user});
});

router.delete('/sessions', [auth], async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Logged out'};

  if(!token) {
    return res.send(success);
  }
  const user = await User.findOne({token});

  if(!user) {
    return res.send(success);
  }

  user.generateToken();
  await user.save();

  return res.send(success);
});

router.get('/', [auth, permit('admin')], async (req, res) => {
  try {
    const user = await User.find();
    return res.send(user)
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.post('/:id', [auth, permit('admin')], async (req, res) => {
  try {
    await req.body;
    await User.updateOne({_id: req.params.id},
      {$set:
          {
            username: req.body.username,
            password: req.body.password,
            role: req.body.role
          }
      });
    res.send({"message": "ok"})
  } catch (error) {
    console.log(error);
    return res.status(400).send(error)
  }
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
  try {
    await User.deleteOne({_id: req.params.id});

    res.send({message: "OK"})

  } catch (e) {
    return res.status(500).send(e);
  }
});


module.exports = router;
