const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CategorySchema = new Schema({
  title: {
    type: String,
    require: true
  }
});

const Category = mongoose.model('Category', CategorySchema);

module.exports = Category;
