const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPathPhotos: path.join(rootPath, 'public/uploads/photos'),
  dbUrl: 'mongodb://localhost/news',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  }
};
