const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const user = require('./app/users');
const article = require('./app/article');
const categories = require('./app/categories');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = process.env.NODE_ENV === 'test' ? 8010 : 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use('/users', user);
  app.use('/article', article);
  app.use('/categories', categories);

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
});
