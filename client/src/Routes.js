import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import MainPage from "./containers/MainPage/MainPage";
import Register from "./containers/User/Register/Register";
import Login from "./containers/User/Login/Login";
import CategoryNews from "./containers/CategoryNews/CategoryNews";
import NotFound from "./containers/NotFound/NotFound";
import AboutNews from "./containers/AboutNews/AboutNews";
import AdminPanel from "./containers/Admin/AdminPanel/AdminPanel";
import CreateCategory from "./containers/Admin/CreateCategory/CreateCategory";
import CreateArticle from "./containers/Admin/CreateArticle/CreateArticle";
import EditCategory from "./containers/Admin/EditCategory/EditCategory";
import EditUsers from "./containers/Admin/EditUsers/EditUsers";
import EditArticle from "./containers/Admin/EditArticle/EditArticle";

const ProtectedRoute = ({isAllowed, ...props}) => (
    isAllowed ? <Route {...props} /> : <Redirect to="/"/>
);

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={MainPage}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/category/:title/:id" exact component={CategoryNews}/>
            <Route path="/article/about/:id" exact component={AboutNews}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'} path="/admin" exact component={AdminPanel}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'} path="/admin/create_users" exact component={Register}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'} path="/admin/create_category" exact component={CreateCategory}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'} path="/admin/create_article" exact component={CreateArticle}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'} path="/admin/edit_category/:title/:id" exact component={EditCategory}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'} path="/admin/edit_users/:username/:role/:id" exact component={EditUsers}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'} path="/admin/edit_article/:id" exact component={EditArticle}/>
            <Route path="/not_found" exact component={NotFound}/>
            <Route component={NotFound}/>
        </Switch>
    );
};

export default Routes;
