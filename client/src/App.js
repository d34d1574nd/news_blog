import React, {Component, Fragment} from 'react';
import {NotificationContainer} from "react-notifications";
import {connect} from "react-redux";

import Routes from "./Routes";

import './App.css';
import {Container} from "reactstrap";
import ToolBar from "./components/UI/ToolBar/ToolBar";
import {logoutUser} from "./store/action/UserAction";
import {fetchTranslate} from "./store/action/googleTranslateActions";
import {fetchCategories} from "./store/action/CategoriesAction";
import Footer from "./components/UI/Footer/Footer";


class App extends Component {
  componentDidMount() {
    this.props.fetchTranslate();
    this.props.fetchCategories();
  }

  render() {
    return (
      <Fragment>
        <ToolBar categories={this.props.categories} user={this.props.user} logout={this.props.logoutUser}/>
        <Container>
          <NotificationContainer/>
          <Routes user={this.props.user}/>
        </Container>
        <Footer/>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser()),
  fetchTranslate: () => dispatch(fetchTranslate()),
  fetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
