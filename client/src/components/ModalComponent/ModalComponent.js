import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import { useSpring, animated } from 'react-spring/web.cjs';

import {history} from "../../store/configureStore";

import CloseCross from "../../assets/close.png";

import "./ModalComponent.css";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const Fade = React.forwardRef(function Fade(props, ref) {
  const { in: open, children, onEnter, onExited, ...other } = props;
  const style = useSpring({
    from: { opacity: 0 },
    to: { opacity: open ? 1 : 0 },
    onStart: () => {
      if (open && onEnter) {
        onEnter();
      }
    },
    onRest: () => {
      if (!open && onExited) {
        onExited();
      }
    },
  });

  return (
    <animated.div ref={ref} style={style} {...other}>
      {children}
    </animated.div>
  );
});

Fade.propTypes = {
  children: PropTypes.element,
  in: PropTypes.bool.isRequired,
  onEnter: PropTypes.func,
  onExited: PropTypes.func,
};

export default function ModalComponent({user, authButton, childrenAuth, buttonText, heading, submitFormHandler, inputChildren}) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(authButton);

  const handleClose = () => {
    setOpen(false);
    if (user && user.role === 'admin') {
      history.push('/admin')
    } else {
      history.push('/')
    }
  };

  return (
    <div>
      <Modal
        aria-labelledby="spring-modal-title"
        aria-describedby="spring-modal-description"
        className={classes.modal}
        open={authButton && open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={authButton && open}>
          <Fragment>
            <div className="social-side">
              <img className="closeButton" src={CloseCross} alt="#"  onClick={handleClose}/>
            </div>
            <div className="fields-side">
              {heading}
              <form onSubmit={submitFormHandler}>
                {inputChildren}
                <Button type="submit" variant="contained" color="primary">
                  {buttonText}
                </Button>
                {childrenAuth}
              </form>
            </div>
          </Fragment>
        </Fade>
      </Modal>
    </div>
  );
}
