import React from 'react';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

const ArticleForm = ({title, description, category_id, imageTitle, categories, inputChangeHandler, submitFormHandler, fileChangeHandler, textButton}) => {
  return (
    <form onSubmit={submitFormHandler}>
      <TextField type='text' id="title" name="title" className="input_form_create" label="Заголовок" variant="outlined" value={title} onChange={inputChangeHandler} required/>
      <TextField
        id="description" name='description'
        label="Описание" className="input_form_create"
        multiline
        rows={20}
        defaultValue={description} onChange={inputChangeHandler}
        variant="outlined"
      />
      <input
        accept="image/*"
        id="image" name="image"
        className="image_article"
        type="file" onChange={fileChangeHandler}
      />
      <label htmlFor="image">
        <Button variant="contained" color="primary" component="span">
          Загрузить изображение
        </Button>
      </label>
      {imageTitle ? <span>{imageTitle}</span> : null}
      <FormControl variant="outlined" className="input_form_create">
        <InputLabel id="demo-simple-select-outlined-label">Категории</InputLabel>
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined" name="category_id"
          value={category_id}
          onChange={inputChangeHandler}
          label="category_id"
        >
          {categories && categories.map((category, index) => (
            <MenuItem key={index} value={category._id} >{category.title}</MenuItem>
          ))}
        </Select>
      </FormControl>
      <Button color="primary" type="submit">{textButton}</Button>
    </form>
  );
};

export default ArticleForm;
