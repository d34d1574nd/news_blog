import React from 'react';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import {apiURL} from "../../constants";

import {NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import CardActionArea from "@material-ui/core/CardActionArea";

import './CardBox.css';

const CardBox = ({title, description, image, date, article_id, children}) => {
  return (
    <Card className="MainPage_cardNews" >
      <NavLink exact tag={RouterNavLink} className="MainPage_cardNewsLink" to={`/article/about/${article_id}`}>
      <CardActionArea>
        <CardMedia
          className="card_media"
          image={`${apiURL}/uploads/photos/${image}`}
          title={title}
        />
        <CardContent>
          <Typography className="news_paragraph_title" gutterBottom variant="h4" component="h2">
            {title}
          </Typography>
            <Typography variant="body2" className="news_paragraph" color="textSecondary"  component="p">
              {description}
            </Typography>
          <Typography variant="body2" className="news_paragraph_date" color="textSecondary"  component="p">
            {date}
          </Typography>
        </CardContent>
      </CardActionArea>
  </NavLink>
      {children}
    </Card>

  );
};

export default CardBox;
