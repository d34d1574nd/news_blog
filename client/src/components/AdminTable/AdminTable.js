import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import Paper from '@material-ui/core/Paper';


export default function AdminTable({tableRow, typeMapData}) {

  return (
    <TableContainer component={Paper}>
      <Table size="small" aria-label="a dense table">
        <TableHead>
          {tableRow}
        </TableHead>
        <TableBody>
          {typeMapData}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
