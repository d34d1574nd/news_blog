import React, {Fragment} from 'react';
import {FormFeedback, FormGroup, Input} from "reactstrap";

const FormElement = ({propertyName, title, error, children, ...props}) => {
    return (
        <Fragment>
          <FormGroup>
            <Input
              name={propertyName} id={propertyName}
              invalid={!!error}
              {...props}
            >
              {children}
            </Input>
            {error && (
              <FormFeedback>
                {error}
              </FormFeedback>
            )}
          </FormGroup>
        </Fragment>
    );
};

export default FormElement;
