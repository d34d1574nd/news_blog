import React from 'react';
import Typography from "@material-ui/core/Typography";
import {NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";

import './Footer.css';

const Footer = () => {
  return (
    <footer>
      <Container>
        <div className="footer_text_center">
          <Typography variant="h6" className="ToolBar_title">
            <NavLink to='/' exact tag={RouterNavLink} className="ToolBar_logo">
              News
            </NavLink>
          </Typography>
          <Box>
            <p className="footer_text">
            news - ведущее информационное агентство, публикует последние новости Кыргызстана.
            Мы придерживаемся высоких стандартов, оперативны и нейтральны.
            </p>
            <p  className="footer_text">
            +996 312 98-69-70, info@knews.kg
            </p>
            <p className="footer_text">Кыргызская Республика, г. Бишкек</p>
            <p className="footer_text">
            Редакция не несет ответственности за содержимое перепечатанных материалов и высказывания отдельных лиц.
            При перепечатке материалов сайта, активная гиперссылка обязательна.
            © 2020, news
            </p>
          </Box>
        </div>
      </Container>
    </footer>
  );
};

export default Footer;
