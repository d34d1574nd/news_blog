import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

import UserMenu from "./UserMenu/UserMenu";
import AuthMenu from "./AuthMenu/AuthMenu";

import './ToolBar.css';
import MenuComponent from "./MenuComponent/MenuComponent";


const ToolBar = ({categories, user, logout}) => {
  return (
    <div className="ToolBar">
      <AppBar position="static">
        <Toolbar>
          <MenuComponent categories={categories}/>
            <Typography variant="h6" className="ToolBar_title">
              <NavLink to='/' exact tag={RouterNavLink} className="ToolBar_logo">
                News
              </NavLink>
            </Typography>
          {user ? <UserMenu user={user} logout={logout}/> : <AuthMenu/>}
          <div id='google_translate_element' className='categories__item'/>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default ToolBar;
