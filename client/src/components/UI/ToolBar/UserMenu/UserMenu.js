import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {NavLink as RouterNavLink} from "react-router-dom";
import {NavLink} from "reactstrap";
import Avatar from "@material-ui/core/Avatar";
import { makeStyles } from '@material-ui/core/styles';
import { deepOrange } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
  orange: {
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: deepOrange[500],
    marginRight: '5px'
  }
}));

export default function UserMenu({user, logout}) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button  aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <Avatar className={classes.orange}>
          {user.username && user.username.charAt(0)}
        </Avatar>
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {user.role === 'admin' ? <NavLink to={'/admin'} tag={RouterNavLink} exact ><MenuItem onClick={handleClose}> Мой кабинет</MenuItem></NavLink> : null}
        <NavLink to='/' tag={RouterNavLink} exact ><MenuItem onClick={() => {handleClose(); logout()}} >Выйти</MenuItem></NavLink>
      </Menu>
    </div>
  );
}
