import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {NavLink as RouterNavLink} from "react-router-dom";
import {NavLink} from "reactstrap";

export default function AuthMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button  aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        Логин
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <NavLink to='/register' tag={RouterNavLink} exact ><MenuItem onClick={handleClose} >Регистрация</MenuItem></NavLink>
        <NavLink to='/login' tag={RouterNavLink} exact ><MenuItem onClick={handleClose} >Авторизация</MenuItem></NavLink>
      </Menu>
    </div>
  );
}
