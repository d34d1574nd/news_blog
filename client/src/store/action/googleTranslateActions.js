const googleTranslateElementInit = () => {
    new window.google.translate.TranslateElement({
        pageLanguage: 'ru',
        autoDisplay: false,
    }, 'google_translate_element')
};

export const fetchTranslate = () => {
    return dispatch => {
        const addScript = document.createElement('script');
        addScript.setAttribute('src', '//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit');
        document.body.appendChild(addScript);
        return window.googleTranslateElementInit = googleTranslateElementInit;
    }
};
