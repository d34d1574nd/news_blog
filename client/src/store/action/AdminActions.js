import {push} from 'connected-react-router'
import {NotificationManager} from 'react-notifications';

import axios from '../../axios-api';

import {fetchCategories} from "./CategoriesAction";
import {fetchArticles} from "./ArticleAction";
import {fetchUsers} from "./UserAction";


export const deleteArticle = article_id => {
  return dispatch => {
    return axios.delete(`/article/${article_id}`).then(
      response => {
        dispatch(fetchArticles());
        dispatch(push(`/admin`));
        NotificationManager.success("Новость удалена")
      }
    )
  }
};

export const deleteCategories = category_id => {
  return dispatch => {
    return axios.delete(`/categories/${category_id}`).then(
      response => {
        dispatch(fetchCategories());
        dispatch(fetchArticles());
        dispatch(push(`/admin`));
        NotificationManager.success("Категория удалена")
      }
    )
  }
};

export const deleteUser = user_id => {
  return dispatch => {
    return axios.delete(`/users/${user_id}`).then(
      response => {
        dispatch(fetchCategories());
        dispatch(fetchArticles());
        dispatch(fetchUsers());
        dispatch(push(`/admin`));
        NotificationManager.success("Пользователь удалён")
      }
    )
  }
};
