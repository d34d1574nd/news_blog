import axios from '../../axios-api';
import {push} from "connected-react-router";
import {NotificationManager} from 'react-notifications';

export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_CATEGORIES_FAILURE = 'FETCH_CATEGORIES_FAILURE';

export const LOADING = 'LOADING';

export const fetchCategoriesSuccess = categories => ({type: FETCH_CATEGORIES_SUCCESS, categories});
export const fetchCategoriesFailure = error => ({type: FETCH_CATEGORIES_FAILURE, error});


export const loading = cancel => ({type: LOADING, cancel});

export const fetchCategories = () => {
  return dispatch => {
    return axios.get('/categories').then(
      response => {
        dispatch(fetchCategoriesSuccess(response.data));
      },
      error => {
        dispatch(fetchCategoriesFailure(error));
      }
    ).finally(() => dispatch(loading(false)))
  }
};

export const createCategory = data => {
  return dispatch => {
    return axios.post('/categories', data).then(
      response => {
        dispatch(fetchCategories());
        dispatch(push('/admin'));
        NotificationManager.success('Категория создана')
      }
    )
  }
};


export const editCategory = (data, id) => {
  return dispatch => {
    return axios.post(`/categories/${id}`, data).then(
      response => {
        dispatch(fetchCategories());
        dispatch(push('/admin'));
        NotificationManager.success('Категория изменена')
      }
    )
  }
};

