import {NotificationManager} from 'react-notifications';
import {push} from 'connected-react-router'

import axios from '../../axios-api';
import {fetchArticles} from "./ArticleAction";
import {loading} from "./CategoriesAction";

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const LOGOUT_USER = 'LOGOUT_USER';

export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_FAILURE = 'FETCH_USERS_FAILURE';

export const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
export const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const fetchUsersSuccess = users => ({type: FETCH_USERS_SUCCESS, users});
export const fetchUsersFailure = error => ({type: FETCH_USERS_FAILURE, error});


export const logoutUser = () => {
    return dispatch => {
        return axios.delete('/users/sessions').then(
            () => {
                dispatch({type: LOGOUT_USER});
                NotificationManager.success('Вы вышли из системы!');
                dispatch(push('/'));
                dispatch(fetchArticles());
            },
            () => {
                NotificationManager.error('Не возможно авторизоваться, попробуйте позже')
            }
        )
    }
};

export const registerUser = (userData, user) => {
    return dispatch => {
      return axios.post('/users', userData).then(
            response => {
              user && user.role === 'admin' ? dispatch(fetchUsers()) : dispatch(registerUserSuccess(response.data.user));
                NotificationManager.success('Регистрация завершена');
              user && user.role === 'admin' ? dispatch(push('/admin')) : dispatch(push('/'))
            },
            error => {
                if (error.response && error.response.data) {
                    dispatch(registerUserFailure(error.response.data));
                  NotificationManager.warning('Такой пользователь уже существует');
                } else {
                    dispatch(registerUserFailure({global: 'Нет соединения'}))
                }
            }
        )
    }
};

export const loginUser = userData => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                NotificationManager.success('Вы успешно вошли в систему!');
                if (response.data.user && response.data.user.role === 'admin') {
                    dispatch(push('/admin'))
                } else {
                    dispatch(push('/'))
                }
            },
            error => {
                if (error.response && error.response.data) {
                    NotificationManager.error('Не правильно введен логин или пароль');
                    dispatch(loginUserFailure(error.response.data));
                  dispatch(push('/'))
                } else {
                    dispatch(loginUserFailure({global: 'Нет соединения'}))
                }
            }
        )
    }
};

export const fetchUsers = () => {
  return dispatch => {
    return axios.get(`/users`).then(
      response => {
        dispatch(fetchUsersSuccess(response.data));
      },
      error => {
        dispatch(fetchUsersFailure(error));
      }
    ).finally(() => dispatch(loading(false)))
  }
};

export const editUsers = (data, id) => {
  return dispatch => {
    return axios.post(`/users/${id}`, data).then(
      response => {
        dispatch(fetchUsers());
        NotificationManager.success('Редактирование завершено');
        dispatch(push('/admin'))
      }
    )
  }
};
