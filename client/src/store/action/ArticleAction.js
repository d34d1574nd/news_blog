import axios from '../../axios-api';
import {push} from "connected-react-router";

import {loading} from "./CategoriesAction";

export const FETCH_ARTICLES_SUCCESS = 'FETCH_ARTICLES_SUCCESS';
export const FETCH_ARTICLES_FAILURE = 'FETCH_ARTICLES_FAILURE';

export const FETCH_CATEGORY_NEWS_SUCCESS = 'FETCH_CATEGORY_NEWS_SUCCESS';
export const FETCH_CATEGORY_NEWS_FAILURE = 'FETCH_CATEGORY_NEWS_FAILURE';

export const FETCH_ABOUT_ARTICLE_SUCCESS = 'FETCH_ABOUT_ARTICLE_SUCCESS';
export const FETCH_ABOUT_ARTICLE_FAILURE = 'FETCH_ABOUT_ARTICLE_FAILURE';

export const fetchArticleSuccess = articles => ({type: FETCH_ARTICLES_SUCCESS, articles});
export const fetchArticleFailure = error => ({type: FETCH_ARTICLES_FAILURE, error});

export const fetchCategoryNewsSuccess = news => ({type: FETCH_CATEGORY_NEWS_SUCCESS, news});
export const fetchCategoryNewsFailure = error => ({type: FETCH_CATEGORY_NEWS_FAILURE, error});

export const fetchAboutNewsSuccess = news => ({type: FETCH_ABOUT_ARTICLE_SUCCESS, news});
export const fetchAboutNewsFailure = error => ({type: FETCH_ABOUT_ARTICLE_FAILURE, error});

export const fetchArticles = () => {
  return dispatch => {
    return axios.get('/article').then(
      response => {
        dispatch(fetchArticleSuccess(response.data));
      },
      error => {
        dispatch(fetchArticleFailure(error));
      }
    ).finally(() => dispatch(loading(false)))
  }
};

export const fetchCategoryArticles = category_id => {
  return dispatch => {
    return axios.get(`/article/category/${category_id}`).then(
      response => {
        dispatch(fetchCategoryNewsSuccess(response.data));
      },
      error => {
        dispatch(fetchCategoryNewsFailure(error));
      }
    ).finally(() => dispatch(loading(false)))
  }
};

export const fetchAboutArticle = article_id => {
  return dispatch => {
    return axios.get(`/article/${article_id}`).then(
      response => {
        dispatch(fetchAboutNewsSuccess(response.data));
      },
      error => {
        dispatch(fetchAboutNewsFailure(error));
      }
    ).finally(() => dispatch(loading(false)))
  }
};

export const createArticle = articleData => {
  return dispatch => {
    return axios.post(`/article`, articleData).then(
      response => {
        dispatch(fetchArticles());
        dispatch(push('/admin'));
      }
    )
  }
};



export const editArticle = (articleData, id) => {
  return dispatch => {
    return axios.post(`/article/${id}`, articleData).then(
      response => {
        dispatch(fetchArticles());
        dispatch(push('/admin'));
      }
    )
  }
};
