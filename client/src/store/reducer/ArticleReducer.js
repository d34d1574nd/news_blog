import {
  FETCH_ABOUT_ARTICLE_FAILURE,
  FETCH_ABOUT_ARTICLE_SUCCESS,
  FETCH_ARTICLES_FAILURE,
  FETCH_ARTICLES_SUCCESS,
  FETCH_CATEGORY_NEWS_FAILURE,
  FETCH_CATEGORY_NEWS_SUCCESS
} from "../action/ArticleAction";

const initialState = {
  articles: null,
  error: null,
  article: null,
  categoryArticles: null
};

const ArticleReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ARTICLES_SUCCESS:
      return {...state, articles: action.articles};
    case FETCH_ARTICLES_FAILURE:
      return {...state, error: action.error, articles: null, article: null};
    case FETCH_CATEGORY_NEWS_SUCCESS:
      return {...state, categoryArticles: action.news};
    case FETCH_CATEGORY_NEWS_FAILURE:
      return {...state, error: action.error, articles: null, article: null, categoryArticles: null};
    case FETCH_ABOUT_ARTICLE_SUCCESS:
      return {...state, article: action.news};
    case FETCH_ABOUT_ARTICLE_FAILURE:
      return {...state, error: action.error, articles: null, article: null, categoryArticles: null};
    default:
      return state
  }
};

export default ArticleReducer;
