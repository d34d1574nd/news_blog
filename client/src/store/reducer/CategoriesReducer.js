import {
  FETCH_CATEGORIES_FAILURE,
  FETCH_CATEGORIES_SUCCESS,
  LOADING
} from "../action/CategoriesAction";

const initialState = {
  categories: null,
  error: null,
  loading: true,
};

const ArticleReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORIES_SUCCESS:
      return {...state, categories: action.categories};
    case FETCH_CATEGORIES_FAILURE:
      return {...state, error: action.error, categories: null};
    case LOADING:
      return {...state, loading: action.cancel};
    default:
      return state
  }
};

export default ArticleReducer;
