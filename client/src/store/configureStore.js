import {connectRouter, routerMiddleware} from "connected-react-router";
import thunkMiddleware from "redux-thunk";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {createBrowserHistory} from "history";

import axios from '../axios-api';
import {loadState, saveState} from "./localStorage";

import userReducer from "./reducer/userReducer";
import articleReducer from "./reducer/ArticleReducer";
import categoriesReducer from "./reducer/CategoriesReducer";
import adminReducer from "./reducer/AdminReducer";

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    user: userReducer,
    articles: articleReducer,
    categories: categoriesReducer,
    admin: adminReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveState({
        user: {
            user: store.getState().user.user
        }
    });
});

axios.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().user.user.token;
    } catch (e) {
        // do nothing, user is not logged in
    }

    return config;
});

export default store;
