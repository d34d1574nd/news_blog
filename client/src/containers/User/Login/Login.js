import React, {Component, Fragment} from 'react';
import {NavLink} from "reactstrap";
import {connect} from "react-redux";
import {NavLink as RouterNavLink} from "react-router-dom";

import FormElement from "../../../components/Form/FormElement/FormElement";
import {loginUser} from "../../../store/action/UserAction";
import ModalComponent from "../../../components/ModalComponent/ModalComponent";

import '../User.css';

class Login extends Component {

    state = {
        password: '',
        username: '',
        open: true
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();
        const data = {
            username: this.state.username,
            password: this.state.password
        };
        this.props.loginUser(data);
        this.setState({
          open: false
        })
    };

    render() {
        const {password, username} = this.state;

        return (
              <ModalComponent heading={<h2>Войти в профиль</h2>} authButton={this.state.open}
                              loginText={'Введите ваш логин'} passText={'Введите ваш пароль'}
                              buttonText={'Войти в профиль'} childrenAuth={
                  <Fragment>
                      <p className={'login-modal-registration'}>
                          <NavLink tag={RouterNavLink}
                                   to={'/register'}
                                   exact
                                   className='password-forgotten-nav-link'
                          >Зарегистрироваться
                          </NavLink>
                      </p>
                  </Fragment>}
                              inputChildren={
                               <Fragment>
                                   <span>Введите ваш логин</span>
                                   <FormElement
                                     propertyName="username"
                                     type="username"
                                     value={username}
                                     required={true}
                                     onChange={this.inputChangeHandler}
                                     placeholder="username"
                                     error={this.props.error &&
                                     this.props.error['non_field_errors'] &&
                                     this.props.error['non_field_errors']}
                                   />
                                   <span>Введите ваш пароль</span>
                                   <FormElement
                                     propertyName="password"
                                     type="password"
                                     value={password}
                                     required={true}
                                     onChange={this.inputChangeHandler}
                                     placeholder="******"
                                     error={this.props.error &&
                                     this.props.error['non_field_errors'] &&
                                     this.props.error['non_field_errors']}
                                   />
                               </Fragment>
                           }
                              submitFormHandler={this.submitFormHandler}
              />
        );
    }
}

const mapStateToProps = state => ({
    error: state.user.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
