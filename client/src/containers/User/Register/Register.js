import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import {registerUser} from "../../../store/action/UserAction";
import FormElement from "../../../components/Form/FormElement/FormElement";
import ModalComponent from "../../../components/ModalComponent/ModalComponent";

import close from "../../../assets/close.png"

import "../User.css";


class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            password: '',
            username: '',
            role: 'user',
        };
    }


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.registerUser({...this.state}, this.props.user);
    };

    getFieldError = fieldName => {
        return this.props.error &&
            this.props.error &&
            this.props.error[fieldName]
    };


    render() {
        const {password, username, role} = this.state;
        return (
          <ModalComponent authButton={true} heading={<h2 >Регистрация пользователя</h2>} user={this.props.user}
                          inputChildren={
                           <Fragment>
                               <span>Введите логин</span>
                               <FormElement
                                 propertyName="username"
                                 type="text"
                                 value={username}
                                 required={true}
                                 onChange={this.inputChangeHandler}
                                 placeholder="username"
                                 error={this.getFieldError('username')}
                               />
                               {this.props.user && this.props.user.role === 'admin' ?
                                <Fragment>
                                    <span>Выберите роль</span>
                                    <FormElement
                                      propertyName="role"
                                      type="select"
                                      value={role}
                                      required={true}
                                      onChange={this.inputChangeHandler}
                                      placeholder="role"
                                      error={this.getFieldError('role')}
                                    >
                                        <option value='user' id="user">user</option>
                                        <option value='admin' id="admin">admin</option>
                                    </FormElement>
                                </Fragment>
                               : null}
                               <span>Придумайте пароль</span>
                               <FormElement
                                 propertyName="password"
                                 type="password"
                                 value={password}
                                 required={true}
                                 onChange={this.inputChangeHandler}
                                 placeholder="******"
                                 error={this.getFieldError('password')}
                               />
                           </Fragment>
                       }
                          passText={'Придумайте пароль'} loginText={'Введите логин'}
                          buttonText={'Далее'} close={close}
                          submitFormHandler={this.submitFormHandler}
          />
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    error: state.user.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: (userData, user) => dispatch(registerUser(userData, user))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
