import React from 'react';

const NotFound = () => {
    return (
        <div style={{paddingTop: '15%'}}>
            <h1 style={{textAlign: 'center'}}>NOT FOUND</h1>
        </div>
    );

};

export default NotFound;