import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

import {fetchAboutArticle} from "../../store/action/ArticleAction";
import {apiURL} from "../../constants";

import './AboutNews.css';

class AboutNews extends Component {
  componentDidMount() {
    this.props.fetchAboutArticle(this.props.match.params.id)
  }

  render() {
    return (
      <Fragment>
        <Typography gutterBottom variant="h4" component="h2" className="about_title">
          {this.props.article && this.props.article.title}
        </Typography>
        <Card className='about_news_card'>
          <CardActionArea>
            <CardMedia
              className="about_news_media"
              image={`${apiURL}/uploads/photos/${this.props.article && this.props.article.image}`}
              title={this.props.article && this.props.article.title}
            />
            <CardContent>
              <Typography variant="body2" color="textSecondary" component="p">
                {this.props.article && this.props.article.description}
              </Typography>
              <Typography variant="body2" className="news_paragraph_date" color="textSecondary" component="span">
                {this.props.article && this.props.article.date}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  article: state.articles.article,
  loading: state.categories.loading
});

const mapDispatchToProps = dispatch => ({
  fetchAboutArticle: article_id => dispatch(fetchAboutArticle(article_id))
});

export default connect(mapStateToProps, mapDispatchToProps)(AboutNews);
