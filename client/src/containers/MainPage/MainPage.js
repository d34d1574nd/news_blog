import React, {Component} from 'react';
import {connect} from "react-redux";

import {fetchArticles} from "../../store/action/ArticleAction";
import CardBox from "../../components/CardBox/CardBox";

import './MainPage.css';
import Spinner from "../../components/UI/Spinner/Spinner";


class MainPage extends Component {

  componentDidMount() {
    this.props.fetchArticles()
  }

  render() {
    if (this.props.loading) {
      return <Spinner />;
    }
    let articles = null;
    if (this.props.articles) {
      articles = this.props.articles.map((article, index) => (
        <CardBox key={index} article_id={article._id} user={this.props.user} className="MainPage_cardNews" title={article.title} description={article.description} author={article && article.user_id} image={article.image} date={article.date}/>
      ))
    }
    return (
      <div className="MainPage">
        {articles}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  articles: state.articles.articles,
  loading: state.categories.loading
});

const mapDispatchToProps = dispatch => ({
  fetchArticles: () => dispatch(fetchArticles()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
