import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import FormElement from "../../../components/Form/FormElement/FormElement";
import ModalComponent from "../../../components/ModalComponent/ModalComponent";

import {createCategory} from "../../../store/action/CategoriesAction";



class CreateCategory extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '',
    };
  }


  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();
    this.props.createCategory({...this.state});
  };

  getFieldError = fieldName => {
    return this.props.error &&
      this.props.error &&
      this.props.error[fieldName]
  };


  render() {
    const {title} = this.state;
    return (
      <ModalComponent authButton={true} heading={<h2 >Создание категории</h2>} user={this.props.user}
                      inputChildren={
                        <Fragment>
                          <span>Введите логин</span>
                          <FormElement
                            propertyName="title"
                            type="text"
                            value={title}
                            required={true}
                            onChange={this.inputChangeHandler}
                            placeholder="Категория"
                            error={this.getFieldError('title')}
                          />
                        </Fragment>
                      }
                      buttonText={'Создать'}
                      submitFormHandler={this.submitFormHandler}
      />
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  error: state.user.registerError
});

const mapDispatchToProps = dispatch => ({
  createCategory: data => dispatch(createCategory(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateCategory);
