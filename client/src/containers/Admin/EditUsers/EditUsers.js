import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import ModalComponent from "../../../components/ModalComponent/ModalComponent";
import FormElement from "../../../components/Form/FormElement/FormElement";
import close from "../../../assets/close.png";
import {editUsers} from "../../../store/action/UserAction";

class EditUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      role: ''
    }
  }
  componentDidMount() {
    this.setState({
      username: this.props.match.params.username,
      role: this.props.match.params.role
    })
  }

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();
    this.props.editUsers({...this.state}, this.props.match.params.id)
  };

  getFieldError = fieldName => {
    return this.props.error &&
      this.props.error &&
      this.props.error[fieldName]
  };
  render() {
    console.log(this.state);
    const {username, role} = this.state;
    return (
      <ModalComponent authButton={true} heading={<h2 >Редактирование пользователя</h2>} user={this.props.user}
                      inputChildren={
                        <Fragment>
                          <span>Введите логин</span>
                          <FormElement
                            propertyName="username"
                            type="text"
                            value={username}
                            required={true}
                            onChange={this.inputChangeHandler}
                            placeholder="username"
                            error={this.getFieldError('username')}
                          />
                              <span>Выберите роль</span>
                              <FormElement
                                propertyName="role"
                                type="select"
                                value={role}
                                required={true}
                                onChange={this.inputChangeHandler}
                                placeholder="role"
                                error={this.getFieldError('role')}
                              >
                                <option value='user'>user</option>
                                <option value='admin'>admin</option>
                              </FormElement>
                        </Fragment>
                      }
                      buttonText={'Сохранить'} close={close}
                      submitFormHandler={this.submitFormHandler}
      />
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
});

const mapDispatchToProps = dispatch => ({
  editUsers: (data, id) => dispatch(editUsers(data, id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditUsers);
