import React, {Component} from 'react';
import {connect} from "react-redux";
import {editArticle, fetchAboutArticle} from "../../../store/action/ArticleAction";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import ArticleForm from "../../../components/ArticleForm/ArticleForm";
import {fetchCategories} from "../../../store/action/CategoriesAction";

class EditArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      image: null,
      category_id: '',
      imageTitle: ''
    };
  }

  componentDidMount = async () => {
    await this.props.fetchAboutArticle(this.props.match.params.id);
    await this.props.fetchCategories();
    await this.setState({
      title: this.props.article && this.props.article.title,
      description: this.props.article && this.props.article.description,
      image: this.props.article && this.props.article.image,
      category_id: this.props.article && this.props.article.category_id && this.props.article.category_id._id
    })
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.match.params.id) {
      if (this.props.match.params.id !== prevProps.match.params.id ) {
        this.props.fetchAboutArticle(this.props.match.params.id);
        this.setState({
          title: this.props.article && this.props.article.title,
          description: this.props.article && this.props.article.description,
          image: this.props.article && this.props.article.image,
          category_id: this.props.article && this.props.article.category_id && this.props.article.category_id._id
        })
      }
    }
  }


  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  fileChangeHandler = event => {
    event.preventDefault();
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        image: file,
        imageTitle: file.name
      });
    };

    reader.readAsDataURL(file);
  };

  submitFormHandler = event => {
    event.preventDefault();
      let formData;
    if (this.state.imageTitle !== '') {
      formData = new FormData();
      Object.keys(this.state).forEach(key => {
        formData.append(key, this.state[key]);
      });
    } else {
      formData = {...this.state}
    }

    this.props.editArticle(formData, this.props.match.params.id);
  };

  render() {
    console.log(this.state.category_id);
    const {title, description, category_id} = this.state;
    return (
      <Box p={3}>
        <Typography variant="h3" className="ToolBar_title">Редактирование новости
        </Typography>
        <ArticleForm
          title={title} description={description}
          category_id={category_id}
          categories={this.props.categories}
          imageTitle={this.props.imageTitle}
          submitFormHandler={this.submitFormHandler}
          inputChangeHandler={this.inputChangeHandler}
          fileChangeHandler={this.fileChangeHandler}
          textButton="Редактировать"
        />
      </Box>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user.user,
  article: state.articles.article,
  categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
  fetchCategories: () => dispatch(fetchCategories()),
  fetchAboutArticle: article_id => dispatch(fetchAboutArticle(article_id)),
  editArticle: (articleData, id) => dispatch(editArticle(articleData, id)),

});

export default connect(mapStateToProps, mapDispatchToProps)(EditArticle);
