import React, {Component} from 'react';
import {NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import {connect} from "react-redux";
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import CreateIcon from '@material-ui/icons/Create';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import CardActions from "@material-ui/core/CardActions";

import TabPanel from "../../../components/TapPanel/TabPanel";
import {fetchUsers} from "../../../store/action/UserAction";
import AdminTable from "../../../components/AdminTable/AdminTable";
import CardBox from "../../../components/CardBox/CardBox";
import {deleteArticle, deleteCategories, deleteUser} from "../../../store/action/AdminActions";
import {fetchArticles} from "../../../store/action/ArticleAction";
import {fetchCategories,} from "../../../store/action/CategoriesAction";
import EditCategory from "../EditCategory/EditCategory";

class AdminPanel extends Component {
  state = {
    tabs: 0
  };

  componentDidMount() {
    this.props.fetchUsers();
    this.props.fetchArticles();
    this.props.fetchCategories();
  }

  a11yProps = index => {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  };

  handleChange = (event, newValue) => {
    this.setState({tabs: newValue});
  };

  editCategory = category => {
    return <EditCategory category={category}/>
  };

  render() {
    return (
      <div>
        <AppBar position="static">
          <Tabs value={this.state.tabs} onChange={this.handleChange} aria-label="simple tabs example">
            <Tab label="Пользователи" {...this.a11yProps(0)} />
            <Tab label="Категории" {...this.a11yProps(1)} />
            <Tab label="Новости" {...this.a11yProps(2)} />
          </Tabs>
        </AppBar>
        <TabPanel value={this.state.tabs} index={0}>
          <NavLink to='/admin/create_users' exact tag={RouterNavLink}>Создать пользователя</NavLink>
          <AdminTable
            tableRow={
              <TableRow>
                <TableCell>Пользователь</TableCell>
                <TableCell align="right">Роль</TableCell>
                <TableCell align="right">Редактировать</TableCell>
                <TableCell align="right">Удалить</TableCell>
              </TableRow>
            }
            typeMapData={
              this.props.users && this.props.users.map(user => {
                return (
                  user.username === this.props.user.username ? null :
                    <TableRow key={user._id}>
                      <TableCell component="th" scope="row">
                        {user.username}
                      </TableCell>
                      <TableCell align="right">{user.role}</TableCell>
                      <TableCell align="right">
                        <NavLink to={`/admin/edit_users/${user.username}/${user.role}/${user._id}`} exact tag={RouterNavLink}>
                          <CreateIcon/>
                        </NavLink>
                      </TableCell>
                      <TableCell align="right">
                        <Button color="secondary" onClick={() => this.props.deleteUser(user._id)}>
                          <DeleteForeverIcon/>
                        </Button>
                      </TableCell>
                    </TableRow>
            )})
            }
          />
        </TabPanel>
        <TabPanel value={this.state.tabs} index={1}>
          <NavLink to='/admin/create_category' exact tag={RouterNavLink}>Создать категорию</NavLink>
          <AdminTable
            tableRow={
              <TableRow>
                <TableCell>Категория</TableCell>
                <TableCell align="right">Редактировать</TableCell>
                <TableCell align="right">Удалить</TableCell>
              </TableRow>
            }
            typeMapData={
              this.props.categories && this.props.categories.map(category => (
                <TableRow key={category._id}>
                  <TableCell component="th" scope="row">
                    {category.title}
                  </TableCell>
                  <TableCell align="right">
                    <NavLink to={`/admin/edit_category/${category.title}/${category._id}`} exact tag={RouterNavLink}>
                      <CreateIcon/>
                    </NavLink>
                  </TableCell>
                  <TableCell align="right">
                    <Tooltip title="При удалении категории удаляться и все новости связанные с ней. Будьте внимательны при удалении" aria-label="delete">
                    <Button color="secondary" onClick={() => this.props.deleteCategories(category._id)}>
                      <DeleteForeverIcon/>
                    </Button>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              ))
            }
          />
        </TabPanel>
        <TabPanel value={this.state.tabs} index={2}>
          <NavLink to='/admin/create_article' exact tag={RouterNavLink}>Написать новость</NavLink>
          <div className="MainPage">
          {this.props.articles && this.props.articles.map((article, index) => (
            <CardBox key={index}
                     deleteArticle={this.props.deleteArticle}
                     article_id={article._id} user={this.props.user}
                     className="MainPage_cardNews" title={article.title}
                     description={article.description}
                     author={article && article.user_id}
                     image={article.image} date={article.date}
                     children={
                           <CardActions>
                             <NavLink to={`/admin/edit_article/${article._id}`} style={{'textTransform': 'uppercase', 'fontWeight': 'bold'}} color="primary" exact tag={RouterNavLink}>
                               Редактировать
                             </NavLink>
                             <Button size="small" color="secondary" onClick={() => this.props.deleteArticle(article._id)}>
                               Удалить
                             </Button>
                           </CardActions>
                           }
            />
          ))}
          </div>
        </TabPanel>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  users: state.user.users,
  articles: state.articles.articles,
  categories: state.categories.categories,
  loading: state.categories.loading
});

const mapDispatchToProps = dispatch => ({
  fetchUsers: () => dispatch(fetchUsers()),
  deleteArticle: article_id => dispatch(deleteArticle(article_id)),
  deleteCategories: category_id => dispatch(deleteCategories(category_id)),
  deleteUser: user_id => dispatch(deleteUser(user_id)),
  fetchArticles: () => dispatch(fetchArticles()),
  fetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminPanel);
