import React, {Component} from 'react';
import {connect} from "react-redux";
import Box from "@material-ui/core/Box";

import {fetchCategories} from "../../../store/action/CategoriesAction";
import {createArticle} from "../../../store/action/ArticleAction";

import './CreateArticle.css';
import Typography from "@material-ui/core/Typography";
import ArticleForm from "../../../components/ArticleForm/ArticleForm";


class CreateArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      image: null,
      category_id: '',
      imageTitle: ''
    };
  }

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  fileChangeHandler = event => {
  event.preventDefault();
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        image: file,
        imageTitle: file.name
      });
    };

    reader.readAsDataURL(file);
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.createArticle(formData);
  };


  render() {
    const {title, description, category_id} = this.state;
    return (
      <Box p={3}>
        <Typography variant="h3" className="ToolBar_title">Создание новости
        </Typography>
        <ArticleForm
          title={title} description={description}
          category_id={category_id}
          categories={this.props.categories}
          imageTitle={this.props.imageTitle}
          submitFormHandler={this.submitFormHandler}
          inputChangeHandler={this.inputChangeHandler}
          fileChangeHandler={this.fileChangeHandler}
          textButton="Создать"
        />
      </Box>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
  fetchCategories: () => dispatch(fetchCategories()),
  createArticle: article => dispatch(createArticle(article))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateArticle);
