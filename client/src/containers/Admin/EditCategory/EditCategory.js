import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import {editCategory} from "../../../store/action/CategoriesAction";
import ModalComponent from "../../../components/ModalComponent/ModalComponent";
import FormElement from "../../../components/Form/FormElement/FormElement";

class EditCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: null
    }
  }
  componentDidMount() {
    this.setState({
      title: this.props.match.params.title
    })
  }

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();
    this.props.editCategory({...this.state}, this.props.match.params.id)
  };

  getFieldError = fieldName => {
    return this.props.error &&
      this.props.error &&
      this.props.error[fieldName]
  };

  render() {
    return (
      <ModalComponent authButton={true} heading={<h2 >Редактирование категории</h2>} user={this.props.user}
                      inputChildren={
                        <Fragment>
                          <span>Редактирование категории</span>
                          <FormElement
                            propertyName="title"
                            type="text"
                            value={this.state.title}
                            required={true}
                            onChange={this.inputChangeHandler}
                            placeholder="Категория"
                            error={this.getFieldError('title')}
                          />
                        </Fragment>
                      }
                      buttonText={'Редактировать'}
                      submitFormHandler={this.submitFormHandler}
      />
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
});

const mapDispatchToProps = dispatch => ({
  editCategory: (data, id) => dispatch(editCategory(data, id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditCategory);
