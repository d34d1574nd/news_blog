import React, {Component} from 'react';
import {connect} from "react-redux";

import {fetchCategoryArticles} from "../../store/action/ArticleAction";
import Spinner from "../../components/UI/Spinner/Spinner";
import CardBox from "../../components/CardBox/CardBox";

class CategoryNews extends Component {
  componentDidMount() {
    this.props.fetchCategoryArticles(this.props.match.params.id)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.match.params.id) {
      if (this.props.match.params.id !== prevProps.match.params.id ) {
        this.props.fetchCategoryArticles(this.props.match.params.id)
      }
    }
  }

  render() {
    if (this.props.loading) {
      return <Spinner />;
    }
    let categoryArticles = null;
    if (this.props.categoryArticles) {
      categoryArticles = this.props.categoryArticles.map((article, index) => (
        <CardBox key={index} article_id={article._id} user={this.props.user} className="MainPage_cardNews" title={article.title} description={article.description} author={article && article.user_id} image={article.image} date={article.date}/>
      ))
    }
    return (
      <div className="MainPage">
        {categoryArticles}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  categoryArticles: state.articles.categoryArticles,
  loading: state.categories.loading
});

const mapDispatchToProps = dispatch => ({
  fetchCategoryArticles: category_id => dispatch(fetchCategoryArticles(category_id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryNews);
