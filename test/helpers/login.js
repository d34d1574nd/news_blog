
const Helper = codeceptjs.helper;

const userData = {
  'admin': {
    'username': 'admin',
    'password': '12345678aA'
  },
  'user': {
    'username': 'user',
    'password': '12345678aA'
  }
};

class LoginHelper extends Helper {

  async loginAsUser(name) {
    const user = userData[name];

    if (!user) throw new Error('No such user is known! Check helpers/login.js');

    const I = this.helpers['Puppeteer'];

    await I.click(button.loginBtn);
    await I.click(button.authBtn);

    const alreadyLoggedIn = await I._locate(element.avatar);

    if (alreadyLoggedIn.length > 0) {
      return;
    }

    const someoneIsLoggedIn = await I._locate(element.avatar);

    if (someoneIsLoggedIn.length > 0) {
      await I.click(element.avatar);
      await I.click(button.outProfile);
    }


    await I.fillField(`//input[@id='username']`, user.username);
    await I.fillField(`//input[@id='password']`, user.password);

    await I.click(button.inProfile);

    await I.waitForText(`Вы успешно вошли в систему!`, 2);
  }
}

module.exports = LoginHelper;
