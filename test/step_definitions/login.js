const { I } = inject();
// Add in your custom step files

Given('я авторизованный пользователь {string}', name => {
  I.amOnPage('/');
  I.waitForElement(button.loginBtn);
  I.loginAsUser(name);
});

When('я переключаю вкладки панели', () => {
  I.click({xpath: `//button[@id='simple-tab-0'][.='Пользователи']`});
  I.click({xpath: `//button[@id='simple-tab-1'][.='Категории']`});
  I.click({xpath: `//button[@id='simple-tab-2'][.='Новости']`});
});
