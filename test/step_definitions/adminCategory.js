const {I} = inject();
// Add in your custom step files

Then('я нажимаю вкладку категория и кнопку создать категорию', () => {
  I.click({xpath: `//button[@id='simple-tab-1']//span[@class='MuiTab-wrapper'][.='Категории']`});
  I.click({xpath: `//a[.='Создать категорию']`});
  I.fillField({xpath: `//input[@id='title']`},  'Хобости');
  I.click({xpath: `//button[@class='MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary']//span[@class='MuiButton-label'][.='Создать']`});
});

Then('я ввожу данные и вижу что категория создана', () => {
  I.waitForText("Категория создана");
  I.click({xpath: `//button[@id='simple-tab-1']//span[@class='MuiTab-wrapper'][.='Категории']`});
  I.waitForVisible({xpath: `//th[.='Хобости']`});
});








