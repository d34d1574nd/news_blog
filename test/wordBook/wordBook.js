const Helper = codeceptjs.helper;

button = {
  loginBtn: "//span[.='Логин']", //кнопка регистрации, авторизации
  regBtn: "//li[.='Регистрация']", //кнопка регистрации, авторизации
  authBtn: "//li[.='Авторизация']", //кнопка регистрации, авторизации
  langBtn: "//select[@class='goog-te-combo']", //кнопка перевода
  inProfile: "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary']//span[@class='MuiButton-label'][.='Войти в профиль']", //кнопка войти
  regProfile: "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary']//span[@class='MuiButton-label'][.='Далее']", //кнопка далее
  outProfile: "//a[@class='nav-link']//li[@class='MuiButtonBase-root MuiListItem-root MuiMenuItem-root MuiMenuItem-gutters MuiListItem-gutters MuiListItem-button'][.='Выйти']", //выйти из профиля
  adminPro: "//a[@class='nav-link active']//li[@class='MuiButtonBase-root MuiListItem-root MuiMenuItem-root MuiMenuItem-gutters MuiListItem-gutters MuiListItem-button']", //профиль админа
  category: "//span[@class='MuiIconButton-label'][.='Категории']", // кнопка категории
};

element = {
  modalTextAuth: "//div[@class='fields-side']//h2[.='Войти в профиль']", //текст в модальном окне авторизации
  modalTextReg: "//div[@class='fields-side']//h2[.='Регистрация пользователя']", //текст в модальном окне регистрации
  avatar: "//div[@class='MuiAvatar-root MuiAvatar-circle makeStyles-orange-13 MuiAvatar-colorDefault']", //аватар
};

link = {
  home: '/',
  login: '/login',
  register: '/register',
};

class WordBookHelper extends Helper {

  async seeNotification(text) {
    const I = this.helpers['Puppeteer'];
    await I.waitForElement(`//div[contains(@class, 'notification-container')]/span[contains(., '${text}')]`, 10);
  };

}

module.exports = WordBookHelper;
