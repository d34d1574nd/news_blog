const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// HEADLESS=true npx codecept run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './*_test.js',
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'http://localhost:3000',
      show: true,
      windowSize: '1400x900'
    },
    LoginHelper: {
      require: './helpers/login.js',
    },
    WordBookHelper: {
      require: './wordBook/wordBook.js'
    },
  },
  include: {
    I: './steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'bookingTest',
  translation: 'ru-RU',
  gherkin: {
    features: './features/*.feature',
    steps: [
      './step_definitions/register.js',
      './step_definitions/registerFail.js',
      './step_definitions/login.js',
      './step_definitions/admin.js',
      './step_definitions/adminRegister.js',
      './step_definitions/adminCategory.js',
    ]
  },
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}
